package com.tbc.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbc.model.Comment;
import com.tbc.model.Post;

@Repository
public interface CommentRepository extends JpaRepository<Comment,Long>{

	List<Comment> findByPost(Post post);

	

}
