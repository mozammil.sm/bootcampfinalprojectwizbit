package com.tbc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbc.model.FriendList;
import com.tbc.model.User;

@Repository
public interface FreindListRepository extends JpaRepository<FriendList,Long> {

	List<FriendList> findByUser1(User user);

	List<FriendList> findByUser2(User user);

}
