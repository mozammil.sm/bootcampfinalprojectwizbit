package com.tbc.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="friend_list")

public class FriendList {
	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)//GenerationType.AUTO
	private long id;
	
	public long getId() {
		return id;
	}
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user1",referencedColumnName="id")
	private User user1;
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user2",referencedColumnName="id")
	private User user2;
	public void setId(long id) {
		this.id = id;
	}
	public User getUser1() {
		return user1;
	}
	public void setUser1(User user1) {
		this.user1 = user1;
	}
	public User getUser2() {
		return user2;
	}
	public void setUser2(User user2) {
		this.user2 = user2;
	}
}
