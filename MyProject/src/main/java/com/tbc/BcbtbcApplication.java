package com.tbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
@SpringBootApplication
public class BcbtbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(BcbtbcApplication.class, args);
	}
}
