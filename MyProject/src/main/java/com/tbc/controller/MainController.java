package com.tbc.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Param;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.tbc.model.BtcPurchaseTable;
import com.tbc.model.Comment;
import com.tbc.model.CustomerData;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentCreateParams;
import com.tbc.dto.EmailDetailsDto;
import com.tbc.dto.UserRegistrationDto;
import com.tbc.model.EmailDetails;
import com.tbc.model.FriendList;
import com.tbc.model.KycTable;
import com.tbc.model.Post;
import com.tbc.model.PurchaseTable;
import com.tbc.model.ReferralTable;
import com.tbc.model.ReferralTransactions;
import com.tbc.model.TbcTransaction;
import com.tbc.model.User;
import com.tbc.repository.BtcPurchaseTableRepository;
import com.tbc.repository.CommentRepository;
import com.tbc.repository.FreindListRepository;
import com.tbc.repository.KycRepository;
import com.tbc.repository.PostRepository;
import com.tbc.repository.PurchaseTableRepository;
import com.tbc.repository.ReferTranRepo;
import com.tbc.repository.ReferralRepository;
import com.tbc.repository.TbcTransactionRepository;
import com.tbc.repository.UserRepository;
import com.tbc.service.EmailService;
import com.tbc.service.UserService;

import net.bytebuddy.utility.RandomString;

@Controller
public class MainController {
	@Autowired
	FreindListRepository friendListRepository;	

	@Autowired
	private CommentRepository commentRepository;
	@Autowired
	private TbcTransactionRepository tbctr;
	@Autowired
	private BtcPurchaseTableRepository btcptr;
	@Autowired
	private UserService userService;
	@Autowired
	private PostRepository postRepository;
	@Autowired
	private EmailService emailService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PurchaseTableRepository ptr;
	@Autowired
	private ReferralRepository rr;
	@Autowired
	private ReferTranRepo rtr;
	@Autowired
	KycRepository kycRepo;
	@Autowired
	@Value("${stripe.apikey}")
	String stripeKey;

	@Value("${project.kycdoc}")
	String path;

	public User loginDetails() {
		AbstractAuthenticationToken auth = (AbstractAuthenticationToken) SecurityContextHolder.getContext()
				.getAuthentication();
		User user = userRepository.findByMail(auth.getName());
		return user;
	}

	@GetMapping("/createPost")
	public String createPost(Model model)
	{
		model.addAttribute("user", loginDetails());
		return "createAPost";

	}
	public String uploadDoc(MultipartFile file) throws IOException {
		String randomCode = RandomString.make(30);

		String fileName = randomCode + file.getOriginalFilename(); // 30digit+OriginalFilename
		String filePath = path + File.separator + fileName;

		File f = new File(path);
		if (!f.exists())
			f.mkdir();

		Files.copy(file.getInputStream(), Paths.get(filePath));

		return fileName;
	}

	@PostMapping("/kyc")
	public String aadharFrontUpload(@RequestParam("profilePic") MultipartFile profilePic,
			@RequestParam("aadharFront") MultipartFile aadharFront,
			@RequestParam("aadharBack") MultipartFile aadharBack, @RequestParam("address") String address) {
		Long id = loginDetails().getId();
		kycVerification(id, profilePic, address, aadharFront, aadharBack);
		return "redirect:/profile";
	}
	@PostMapping("/createPost")
	public String createPost(@RequestParam("postName") MultipartFile postName,@RequestParam("caption") String caption) throws IOException {
		System.out.println("cretae poste");
		Long id = loginDetails().getId();
		createpost(id, postName, caption);
		return "redirect:/profile";
	}

	@GetMapping("/findFriends")
	public String findFrineds(Model model)
	{
		User user=loginDetails();
		model.addAttribute(loginDetails());
		List<FriendList> friend1=friendListRepository.findByUser1(user);
		List<FriendList> friend2=friendListRepository.findByUser2(user);
		List<User>allUser=userRepository.findAll();
		System.out.println(allUser);
		for(int i=0;i<friend1.size();i++)
		{
			if(allUser.contains(friend1.get(i).getUser2())) {
				allUser.remove(friend1.get(i).getUser2());
			}
		}
		for(int i=0;i<friend2.size();i++)
		{
			if(allUser.contains(friend2.get(i).getUser1())) {
				allUser.remove(friend2.get(i).getUser1());
			}
		}
		System.out.println(allUser);
		model.addAttribute("alluser",allUser);
		return "findFriends";
	}
	@PostMapping("/buy2")
	public String buy2(@ModelAttribute("user")UserRegistrationDto registrationDto,Model model,ModelMap map) throws StripeException {
		System.out.println("in post"+registrationDto.getAmount());
		int price=registrationDto.getAmount()*5;
		if(price<0) {
			return "redirect:/manualBuy?lessPrice";
		}
		Stripe.apiKey = stripeKey;
		User user = loginDetails();
		CustomerData customer = new CustomerData();
		customer.setAmount((long)price);
		customer.setEmail(user.getMail());
		model.addAttribute("c", customer);
		PaymentIntentCreateParams params = PaymentIntentCreateParams.builder().setAmount((long) price* 100)
				.setCurrency("inr").addPaymentMethodType("card").setCustomer(user.getStripeId()).build();
		PaymentIntent paymentIntent = PaymentIntent.create(params);
		map.put("clientSecret", paymentIntent.getClientSecret());
		return "dashboardBuy";
	}
	@PostMapping("/payFromDashboard")
	public @ResponseBody String payFromDashboard(@RequestParam("paymentId") String paymentId, HttpSession httpSession)
			throws StripeException {
		System.out.println("in paywithDashboard");
		PaymentIntent paymentIntent = PaymentIntent.retrieve(paymentId);

		String paymentResponse = paymentIntent.toJson();
		System.out.println(paymentResponse);
		Double purchasePrice = (double) (paymentIntent.getAmount() / 100);
		System.out.println(purchasePrice);
		setBtcTable(purchasePrice);
		setTransactionTable(purchasePrice);
		User user=loginDetails();
		long amount=(long) (purchasePrice/5);
		return "success";
	}
	public void setBtcTable(Double purchasePrice) {
		
		User user = loginDetails();
		int purchaserTbc = (int) (purchasePrice * 20 / 100);
		BtcPurchaseTable bpt=new BtcPurchaseTable();
		bpt.setDate(new Date());
		bpt.setPrice(purchasePrice);
		bpt.setQuantity(purchaserTbc);
		bpt.setStatus("success");
		bpt.setUser(user);
		btcptr.save(bpt);
		
	}
	
public void setTransactionTable(Double purchasePrice) {
		
		User user = loginDetails();
		int purchaseTbc = (int) (purchasePrice * 20 / 100);
		int pp=(int)(purchasePrice*1/1);
		TbcTransaction tbct=new TbcTransaction();
		tbct.setDate(new Date());
		tbct.setPrice(pp);
		tbct.setTbcAmount(purchaseTbc);
		tbct.setStatus("success");
		tbct.setUser(user);
		tbctr.save(tbct);
		
	}
	@GetMapping("/addFriend/{id}")
	public String addFrineds(Model model,@PathVariable(value="id")long id)
	{

		model.addAttribute(loginDetails());
		User user=loginDetails();
		FriendList friendList=new FriendList();
//		friendList.setId( RandomString.make(5));
		friendList.setUser1(user);
		friendList.setUser2(userRepository.findById(id).get());
		friendListRepository.save(friendList);
		List<FriendList> friend1=friendListRepository.findByUser1(user);
		List<FriendList> friend2=friendListRepository.findByUser2(user);

		List<User> friends=new ArrayList<User>();
		for(int i=0;i<friend1.size();i++)
		{
			friends.add(friend1.get(i).getUser2());
		}
		for(int i=0;i<friend2.size();i++)
		{
			friends.add(friend1.get(i).getUser1());
		}
		 HashSet<User> allFriends
         = new HashSet<User>(friends);

		model.addAttribute("friends",allFriends);
		model.addAttribute("alluser",userRepository.findAll());
		return "redirect:/myFriends";
	}
	@GetMapping("/manualBuy")
	public String buyManual(Model model,ModelMap map) {
		User user=loginDetails();
		model.addAttribute("user",loginDetails());
		double usd=user.getTbcBalance()*.61;
		map.put("usd",usd);
		return "manualBuy";
	}
	
	@GetMapping("/myFriends")
	public String myFriends(Model model)
	{
		User user=loginDetails();
		List<FriendList> friend1=friendListRepository.findByUser1(user);
		List<FriendList> friend2=friendListRepository.findByUser2(user);

		List<User> friends=new ArrayList<User>();
		for(int i=0;i<friend1.size();i++)
		{
			friends.add(friend1.get(i).getUser2());
		}
		for(int i=0;i<friend2.size();i++)
		{
			friends.add(friend1.get(i).getUser1());
		}
		 HashSet<User> allFriends
         = new HashSet<User>(friends);
		 model.addAttribute("user", user);
		model.addAttribute("friends",allFriends);
		model.addAttribute("alluser",userRepository.findAll());
		return "myFriends";
	}


	@PostMapping("/comment/{id}")
	public String addComment(@PathVariable(value="id")long id,@ModelAttribute Comment c) {
		System.out.println(c.getText());
		Comment comment=new Comment();
		Optional<Post> post=postRepository.findById(id);
		comment.setText(c.getText());
		comment.setPost(post.get());
		comment.setUser(loginDetails());
		comment.setCreatedAt(LocalDate.now());
		commentRepository.save(comment);
		return "redirect:/post/{id}";

	}

	@GetMapping("/post/{id}")
	public String showPost(@PathVariable(value="id")long id,Model model) {
		System.out.println("photo id "+id);
		Optional<Post> post=postRepository.findById(id);
		model.addAttribute("post", post.get());
		Comment comment=new Comment();
		List<Comment> postComment=commentRepository.findByPost(post.get());
		model.addAttribute("postComment", postComment);
		model.addAttribute("user", loginDetails());
		model.addAttribute(comment);
		return "showPost";

	}
	@GetMapping("/deleteComment/{cid}/post/{id}")
	public String deleteComment(@PathVariable(value="cid")long cid,@PathVariable(value="id")long id,Model model) {
		System.out.println("comment id="+cid);
		model.addAttribute("user", loginDetails());
		Optional<Comment> deletedComment=commentRepository.findById(cid);
		System.out.println(deletedComment.get().getText());
		System.out.println(id);
		commentRepository.delete(deletedComment.get());
		return "redirect:/post/{id}";

	}

	private void createpost(Long id, MultipartFile postName, String caption) throws IOException {
		Post post=new Post();
		String postname=uploadDoc(postName);
		post.setPostName(postname);
		post.setCaption(caption);
		post.setUser(loginDetails());
		post.setCreatedAt(LocalDate.now());
		postRepository.save(post);
	}

	public User kycVerification(Long id, MultipartFile profilePic, String address, MultipartFile aadharFront,
			MultipartFile aadharBack) {
		User kycTbl=loginDetails();
		try {
			String ProfilePicFileName = uploadDoc(profilePic);
			String aadharFrontFileName = uploadDoc(aadharFront);
			String aadharBackFileName = uploadDoc(aadharBack);
			System.out.println(profilePic);
			System.err.println(address);
			kycTbl.setProfileName(ProfilePicFileName);
			kycTbl.setAadharFrontName(aadharFrontFileName);
			kycTbl.setAadharBackName(aadharBackFileName);
			kycTbl.setAddress(address);
			kycTbl.setStatus("uploaded");
			kycTbl.setOutputCode("success");
			kycTbl.setKycPath(path);
			User user=loginDetails();
			user.setAddress(address);
			userRepository.save(user);
			if (kycTbl.getOutputCode().equalsIgnoreCase("Success")) {
				userRepository.save(kycTbl);
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			kycTbl.setStatus("Failure");
		}
		return kycTbl;
	}

	@GetMapping("/login")
	public String login() throws IOException, SchedulerException {
		return "login";
	}

	@GetMapping("/profile")
	public String profile(Model model) {
		KycTable kycTable=new KycTable();
		model.addAttribute("kycTable",kycTable);
		model.addAttribute("user", loginDetails());
		return "app-profile";
	}

	@GetMapping("/kycVerification")
	public String kycVerification(Model model) {
		model.addAttribute("alluser", userRepository.findAll());
		model.addAttribute("user",loginDetails());
		return "kyc-verification";
	}

	@GetMapping("/viewUser/{id}")
	public String viewUser(@PathVariable(value = "id") long id, Model model) throws IOException, SchedulerException {
		model.addAttribute("user", loginDetails());
		Optional<User> user = userRepository.findById(id);
		if (user.isPresent()) {
			System.out.println(user.get());
		}
		System.out.println(user.get().getName());
		ReferralTable rt = rr.findByReferralId(user.get().getReferedByPk());
		System.out.println(rt);
		model.addAttribute("rt", rt);
		model.addAttribute("u", user.get());
		return "viewUser";
	}
	@GetMapping("/verify/{id}")
	public String verify(@PathVariable(value = "id") long id, Model model) throws IOException, SchedulerException {
		Optional<User> user = userRepository.findById(id);
		if (user.isPresent()) {
			System.out.println(user.get());
		}
		user.get().setStatus("verified");
		userRepository.save(user.get());
		return "redirect:/kycVerification?verified";
	}
	@GetMapping("/reject/{id}")
	public String reject(@PathVariable(value = "id") long id, Model model) throws IOException, SchedulerException {
		Optional<User> user = userRepository.findById(id);
		if (user.isPresent()) {
			System.out.println(user.get());
		}
		user.get().setStatus("rejected");
		userRepository.save(user.get());
		return "redirect:/kycVerification?rejected";	}

	@GetMapping("/activateUser/{id}")
	public String activateUser(@PathVariable(value = "id") long id, Model model)
			throws IOException, SchedulerException {
		Optional<User> user = userRepository.findById(id);
		if (user.isPresent()) {
			System.out.println(user.get());
		}
		user.get().setRoles("ROLE_USER");
		user.get().setIsDeleted(0);
		userRepository.save(user.get());
		return "redirect:/userControl?activated";
	}

	@GetMapping("/blockUser/{id}")
	public String blockUser(@PathVariable(value = "id") long id, Model model) throws IOException, SchedulerException {
		Optional<User> user = userRepository.findById(id);
		if (user.isPresent()) {
			System.out.println(user.get());
		}
		user.get().setRoles("ROLE_BLOCKED");
		user.get().setIsDeleted(1);
		userRepository.save(user.get());
		return "redirect:/userControl?blocked";
	}

	@GetMapping("/buy")
	public String buy(Model model, ModelMap map, @RequestParam("param1") int param1)
			throws IOException, SchedulerException, StripeException {
		Stripe.apiKey = stripeKey;
		User user = loginDetails();
		CustomerData customer = new CustomerData();
		customer.setAmount((long) param1);
		customer.setEmail(user.getMail());
		model.addAttribute("c", customer);
		PaymentIntentCreateParams params = PaymentIntentCreateParams.builder().setAmount((long) param1 * 100)
				.setCurrency("inr").addPaymentMethodType("card").setCustomer(user.getStripeId()).build();
		PaymentIntent paymentIntent = PaymentIntent.create(params);
		System.out.println(paymentIntent.getClientSecret());
		map.put("clientSecret", paymentIntent.getClientSecret());
		return "buy500p";

	}

	@RequestMapping("/paymentForm")
	public String paymentForm(@ModelAttribute("c") CustomerData c, Model model, ModelMap map) throws StripeException {
		Stripe.apiKey = stripeKey;
		CustomerData customer = new CustomerData();
		customer.setAmount(c.getAmount());
		customer.setEmail(c.getEmail());
		model.addAttribute("c", customer);
		PaymentIntentCreateParams params = PaymentIntentCreateParams.builder().setAmount(c.getAmount() * 100)
				.setCurrency("inr").addPaymentMethodType("card").setCustomer(loginDetails().getStripeId()).build();
		PaymentIntent paymentIntent = PaymentIntent.create(params);
		System.out.println(paymentIntent.getClientSecret());
		map.put("clientSecret", paymentIntent.getClientSecret());
		return "checkout";
	}

	@PostMapping("/payWithNewCard")
	public @ResponseBody String payWithNewCard(@RequestParam("paymentId") String paymentId, HttpSession httpSession)
			throws StripeException {
		System.out.println("in paywithnewcard");
		PaymentIntent paymentIntent = PaymentIntent.retrieve(paymentId);

		String paymentResponse = paymentIntent.toJson();
		System.out.println(paymentResponse);
		Double purchasePrice = (double) (paymentIntent.getAmount() / 100);
		System.out.println(purchasePrice);
		buy(purchasePrice);
		return "success";
	}

	public void buy(Double purchasePrice) {
		User user = loginDetails();
		double purchaserTbc = purchasePrice * 50 / 100;
		user.setTbcBalance(user.getTbcBalance() + purchaserTbc);
		if (purchasePrice == 100.0) {
			user.setHas100pack("yes");
			user.setHasPurchased("yes");
		}
		if (purchasePrice == 200.0) {
			user.setHas200pack("yes");
			user.setHasPurchased("yes");
		}
		if (purchasePrice == 500.0) {
			user.setHas500Pack("yes");
			user.setHasPurchased("yes");
		}
		if (purchasePrice == 1000.0) {
			user.setHas1000pack("yes");
			user.setHasPurchased("yes");
		}
		if (purchasePrice == 1500.0) {
			user.setHas1500pack("yes");
			user.setHasPurchased("yes");
		}
		if (purchasePrice == 2000.0) {
			user.setHas2000pack("yes");
			user.setHasPurchased("yes");
		}
		if (purchasePrice == 5000.0) {
			user.setHas5000pack("yes");
			user.setHasPurchased("yes");
		}
		if (purchasePrice == 10000.0) {
			user.setHas10000pack("yes");
			user.setHasPurchased("yes");
		}

		setPurchaseTable(purchasePrice, user);
		userRepository.save(user);
		if (user.getReferedByPk() != null && user.getReferedByPk().length() > 1) {

			ReferralTable rl1 = rr.findByReferralId(user.getReferedByPk());
			Optional<User> ul1l = userRepository.findById(rl1.getUser().getId());
			if (!ul1l.isEmpty()) {
				int level = 1;
				User ul1 = ul1l.get();
				System.out.println(ul1.getName());
				double ul1tbc = purchaserTbc * 10 / 100;
				ul1.setTbcBalance(ul1.getTbcBalance() + ul1tbc);
				userRepository.save(ul1);
				setReferralTransaction(level, ul1, ul1tbc, purchasePrice);
				if (ul1.getReferedByPk().length() > 1) {
					ReferralTable rl2 = rr.findByReferralId(ul1.getReferedByPk());

					Optional<User> ul2l = userRepository.findById(rl2.getUser().getId());
					if (!ul2l.isEmpty()) {
						level = 2;
						User ul2 = ul2l.get();
						System.out.println(ul2.getName());
						double ul2tbc = ul1tbc * 10 / 100;
						ul2.setTbcBalance(ul2.getTbcBalance() + ul2tbc);
						userRepository.save(ul2);
						setReferralTransaction(level, ul2, ul2tbc, purchasePrice);
						if (ul2.getReferedByPk().length() > 1) {
							ReferralTable rl3 = rr.findByReferralId(ul2.getReferedByPk());

							Optional<User> ul3l = userRepository.findById(rl3.getUser().getId());
							if (!ul3l.isEmpty()) {
								level = 3;
								User ul3 = ul3l.get();
								System.out.println(ul3.getName());
								Double ul3tbc = ul2tbc * 10 / 100;
								ul3.setTbcBalance(ul3.getTbcBalance() + ul3tbc);
								userRepository.save(ul3);
								setReferralTransaction(level, ul3, ul3tbc, purchasePrice);
							}
						}
					}
				}
			}
		}
	}

	private void setReferralTransaction(int level, User ul1, double i, Double purchasePrice) {
		ReferralTransactions rt = new ReferralTransactions();
		rt.setLevel(level);
		rt.setPlanOfPurchase(purchasePrice);
		rt.setBtcEarned(i);
		rt.setTime(new Date());
		System.out.println(i);
		rt.setUser(ul1);
		rt.setGotFrom(loginDetails());
		rtr.save(rt);
	}

	private void setPurchaseTable(Double purchasePrice, User user) {
		PurchaseTable pt = new PurchaseTable();
		pt.setAmountDeposited(0);
		Date date = new Date();
		pt.setDate(date);
		pt.setPurchasePrice(purchasePrice);
		pt.setStatus("success");
		pt.setUser(user);
		ptr.save(pt);
	}

	@GetMapping("/referralTransactions")
	public String referralTransactions(Model model) {

		User user = loginDetails();
		System.out.println(userService.rtr());
		model.addAttribute("rtr", userService.rtr());
		model.addAttribute("user", loginDetails());
		return "referal-tranasactions";
	}

	@GetMapping("/")
	public String home(Model model) throws IOException, SchedulerException {

		User user = loginDetails();
		System.out.println(user.getRoles());
		model.addAttribute(user);
		System.out.println("logging in");
		model.addAttribute("post",postRepository.findAll());
		if (user.isEnabled()) {
			return "index";
		} else {
			return "login?error";
		}

	}

	@GetMapping("/registerSuccess")
	public String otp(Model model) {
		return "registerSuccess";
	}

	@GetMapping("/getEmailForm")
	public String emailForm(Model model) {

		EmailDetails emailDetails = new EmailDetails();

		User user = loginDetails();

		model.addAttribute(emailDetails);
		model.addAttribute(user);
		return "sendMail";
	}

	@GetMapping("/myWallets")
	public String myWallets(Model model) {
		PurchaseTable pt = new PurchaseTable();
		User user = loginDetails();
		long id = user.getId();
		model.addAttribute("pt", userService.pt());
		model.addAttribute("user", user);
		System.out.println(pt.getAmountDeposited());
		model.addAttribute("allpt", userService.allPt());
		return "my-wallets";
	}

	@GetMapping("/packagePurchased")
	public String packagePurchased(Model model) {
		User user = loginDetails();
		model.addAttribute("user", user);
		model.addAttribute("allpt", userService.allPt());
		return "package-purchased";
	}

	@GetMapping("/transaction")
	public String transaction(Model model) {
		User user = loginDetails();
		model.addAttribute("tt", userService.tt());
		model.addAttribute("user", user);
		return "tranasactions";
	}

	@GetMapping("/userControl")
	public String userControl(Model model) {
		User user = loginDetails();
		model.addAttribute("alluser", userRepository.findAll());
		model.addAttribute("user", user);
		return "user-control";
	}

	@PostMapping("/getEmail")
	public String submitMail(@ModelAttribute("emailDetails") EmailDetailsDto edt, Model model) {

		EmailDetails emailDetails = new EmailDetails();
		System.out.println(edt);
		String status = emailService.sendSimpleMail(edt);
		return "plans";
	}

}

//@GetMapping("/buy500pack")
//public String buy500p(Model model,ModelMap map) throws IOException, SchedulerException, StripeException {
//	Stripe.apiKey=stripeKey;
//	User user=loginDetails();
//	CustomerData customer=new CustomerData();
//	customer.setAmount(500l);
//	customer.setEmail(user.getMail());
//	model.addAttribute("c",customer);
//	PaymentIntentCreateParams params =
//			  PaymentIntentCreateParams.builder()
//			    .setAmount(500l)
//			    .setCurrency("inr")
//			    .addPaymentMethodType("card")
//			    .setCustomer(user.getStripeId())
//			    .build();
//	PaymentIntent paymentIntent = PaymentIntent.create(params);
//	System.out.println(paymentIntent.getClientSecret());
//	map.put("clientSecret", paymentIntent.getClientSecret());
//	return "buy500p";
//}
//@PostMapping("/buy1000")
//public String buy2(@RequestParam("price")int price) {
//	System.out.println(price);
//	AbstractAuthenticationToken auth = (AbstractAuthenticationToken)
//			SecurityContextHolder.getContext().getAuthentication();
//	User user=userRepository.findByMail(auth.getName());
//	user.setTbcBalance(user.getTbcBalance()+500);
//	int purchasePrice=1000;
//	setPurchaseTable(purchasePrice,user);
//	userRepository.save(user);
//	if(user.getReferedByPk().length()>1)
//	{
//
//		ReferralTable rl1=rr.findByReferralId(user.getReferedByPk());
//
//		Optional<User> ul1l=userRepository.findById(rl1.getUser().getId());
//		if(!ul1l.isEmpty())
//		{
//			int level=1;
//			User ul1=ul1l.get();
//			System.out.println(ul1.getName());
//
//			ul1.setTbcBalance(ul1.getTbcBalance()+100);
//			userRepository.save(ul1);
//			setReferralTransaction(level,ul1,100.00,1000);
//			if(ul1.getReferedByPk().length()>1)
//			{
//				ReferralTable rl2=rr.findByReferralId(ul1.getReferedByPk());
//
//				Optional<User> ul2l=userRepository.findById(rl2.getUser().getId());
//				if(!ul2l.isEmpty())
//				{
//					level=2;
//					User ul2=ul2l.get();
//					System.out.println(ul2.getName());
//
//					ul2.setTbcBalance(ul2.getTbcBalance()+10);
//					userRepository.save(ul2);
//					setReferralTransaction(level,ul2,10.00,1000);
//					if(ul2.getReferedByPk().length()>1)
//					{
//						ReferralTable rl3=rr.findByReferralId(ul2.getReferedByPk());
//
//						Optional<User> ul3l=userRepository.findById(rl3.getUser().getId());
//						if(!ul3l.isEmpty())
//						{
//							level=3;
//							User ul3=ul3l.get();
//							System.out.println(ul3.getName());
//
//							ul3.setTbcBalance(ul3.getTbcBalance()+1);
//							userRepository.save(ul3);
//							setReferralTransaction(level,ul3,1.00,1000);	
//						}
//					}
//				}
//			}
//		}
//	}
//	return "redirect:/myWallets?p1000";
//}
//@PostMapping("/buy1500")
//public String buy3() {
//	AbstractAuthenticationToken auth = (AbstractAuthenticationToken)
//			SecurityContextHolder.getContext().getAuthentication();
//	User user=userRepository.findByMail(auth.getName());
//	user.setTbcBalance(user.getTbcBalance()+750);
//	int purchasePrice=1500;
//	setPurchaseTable(purchasePrice,user);
//	userRepository.save(user);
//	if(user.getReferedByPk().length()>1)
//	{
//
//		ReferralTable rl1=rr.findByReferralId(user.getReferedByPk());
//
//		Optional<User> ul1l=userRepository.findById(rl1.getUser().getId());
//		if(!ul1l.isEmpty())
//		{
//			int level=1;
//			User ul1=ul1l.get();
//			System.out.println(ul1.getName());
//
//			ul1.setTbcBalance(ul1.getTbcBalance()+150);
//			userRepository.save(ul1);
//			setReferralTransaction(level,ul1,150.00,1500);
//			if(ul1.getReferedByPk().length()>1)
//			{
//				ReferralTable rl2=rr.findByReferralId(ul1.getReferedByPk());
//
//				Optional<User> ul2l=userRepository.findById(rl2.getUser().getId());
//				if(!ul2l.isEmpty())
//				{
//					level=2;
//					User ul2=ul2l.get();
//					System.out.println(ul2.getName());
//
//					ul2.setTbcBalance(ul2.getTbcBalance()+15);
//					userRepository.save(ul2);
//					setReferralTransaction(level,ul2,15.00,1500);
//					if(ul2.getReferedByPk().length()>1)
//					{
//						ReferralTable rl3=rr.findByReferralId(ul2.getReferedByPk());
//
//						Optional<User> ul3l=userRepository.findById(rl3.getUser().getId());
//						if(!ul3l.isEmpty())
//						{
//							level=3;
//							User ul3=ul3l.get();
//							System.out.println(ul3.getName());
//
//							ul3.setTbcBalance(ul3.getTbcBalance()+1);
//							userRepository.save(ul3);
//							setReferralTransaction(level,ul3,1.50,1500);	
//						}
//					}
//				}
//			}
//		}
//	}
//	return "redirect:/myWallets?p1500";
//}@PostMapping("/buy2000")
//public String buy4() {
//	AbstractAuthenticationToken auth = (AbstractAuthenticationToken)
//			SecurityContextHolder.getContext().getAuthentication();
//	User user=userRepository.findByMail(auth.getName());
//	user.setTbcBalance(user.getTbcBalance()+1000);
//	int purchasePrice=2000;
//	setPurchaseTable(purchasePrice,user);
//	userRepository.save(user);
//	if(user.getReferedByPk().length()>1)
//	{
//
//		ReferralTable rl1=rr.findByReferralId(user.getReferedByPk());
//
//		Optional<User> ul1l=userRepository.findById(rl1.getUser().getId());
//		if(!ul1l.isEmpty())
//		{
//			int level=1;
//			User ul1=ul1l.get();
//			System.out.println(ul1.getName());
//
//			ul1.setTbcBalance(ul1.getTbcBalance()+200);
//			userRepository.save(ul1);
//			setReferralTransaction(level,ul1,200.00,2000);
//			if(ul1.getReferedByPk().length()>1)
//			{
//				ReferralTable rl2=rr.findByReferralId(ul1.getReferedByPk());
//
//				Optional<User> ul2l=userRepository.findById(rl2.getUser().getId());
//				if(!ul2l.isEmpty())
//				{
//					level=2;
//					User ul2=ul2l.get();
//					System.out.println(ul2.getName());
//
//					ul2.setTbcBalance(ul2.getTbcBalance()+20);
//					userRepository.save(ul2);
//					setReferralTransaction(level,ul2,20.00,2000);
//					if(ul2.getReferedByPk().length()>1)
//					{
//						ReferralTable rl3=rr.findByReferralId(ul2.getReferedByPk());
//
//						Optional<User> ul3l=userRepository.findById(rl3.getUser().getId());
//						if(!ul3l.isEmpty())
//						{
//							level=3;
//							User ul3=ul3l.get();
//							System.out.println(ul3.getName());
//
//							ul3.setTbcBalance(ul3.getTbcBalance()+2);
//							userRepository.save(ul3);
//							setReferralTransaction(level,ul3,2.00,2000);	
//						}
//					}
//				}
//			}
//		}
//	}
//	return "redirect:/myWallets?p2000";
//}
//
